import java.util.Scanner;

public class CubeVolume {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    
    System.out.println("Enter the side of the cube:");
    
    double side = scanner.nextDouble();
    double volume = calculateCubeVolume(side);
    
    System.out.println("The volume of the cube is: " + volume);

    scanner.close(); 
  }

  public static double calculateCubeVolume(double side) {
    return Math.pow(side, 3);
  }
}